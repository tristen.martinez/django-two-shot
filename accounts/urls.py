from django.urls import path
from accounts.views import UserLogin, UserLogout, signup

urlpatterns = [
    path("login/", UserLogin.as_view(), name="login"),
    path("logout/", UserLogout.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
